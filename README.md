# ts-recommender-systems

## 介绍
使用天枢深度学习框架实现了6种推荐算法。对于每个模型，我们同时提供了模型的定义、训练以及推理的代码。并且对于每个模型，我们至少提供两个脚本train.sh和infer.sh，分别对应模型的训练和推理，便于使用者快速上手。

## 模型列表

[dcn](https://gitee.com/zhijiangtianshu/ts-recommender-systems/tree/master/dcn)

[deepfm](https://gitee.com/zhijiangtianshu/ts-recommender-systems/tree/master/deepfm)

[dlrm](https://gitee.com/zhijiangtianshu/ts-recommender-systems/tree/master/dlrm)

[pnn](https://gitee.com/zhijiangtianshu/ts-recommender-systems/tree/master/pnn)

[xdeepfm](https://gitee.com/zhijiangtianshu/ts-recommender-systems/tree/master/xdeepfm)

[wide_and_deep](https://gitee.com/zhijiangtianshu/ts-recommender-systems/tree/master/wide_and_deep)

